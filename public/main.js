const update = document.querySelector('#update_button')

update.addEventListener('click', _=>{
    fetch('/', {
        method: 'put',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            name:"Musta",
            quote:"hey bro"
        })
    })
    .then(res => {
        if (res.ok) return res.json()
    })
    .then(response => {
        window.location.reload(true)
    })
})


const delete_button = document.querySelector('#delete_button')

delete_button.addEventListener('click', _=>{
    fetch('/', {
        method: 'delete',
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify({
            name: 'Musta'
        })
    })
    .then(res => {
        if(res.ok) return res.json()
    })
    .then(response => {
        if(response === 'nothing to delete'){
            nessageDiv.textContent = 'Nothing to delete'
        } else{
        window.location.reload(true)
        }
    })
    .catch(error => console.log(error))
})