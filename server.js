const express = require("express");
const app = express();
app.use(express.static("public"));
app.set("view engine", "ejs");


const port = 9369;
const bodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;




////////////////////////////////////////////////////////////////EXTRACT POST REQUEST AND EXPOSES IT TO REQ.BODY//////////////////////////////////////////////////////////////////////////////////////
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

////////////////////////////////////////////////////////////////CONNECT TO DATABASE////////////////////////////////////////////////////////////////
MongoClient.connect(
  "mongodb+srv://erorua16:simplon@cluster0.17vth.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
)
  .then((client) => {
    // console.log("Connected to Database")
    ////////////////////////////////////////////////////////////////DATABASE CRUDE/////////////////////////////////////////////////////////////////////////////////////////////
    //database name
    const db = client.db("todo_list");
    //add a collection
    const task_collection = db.collection("task_name");
    ////////////////////////////////////////////////////////////////REQUESTS EXPRESS////////////////////////////////////////////////////////////////
    //Show index.ejs through express
    app.get("/", (req, res) => {
      db.collection("task_name")
        .find()
        .toArray()
        .then((result) => {
          res.render("index.ejs", { task_name: result });
        })
        .catch((error) => console.log(error));
    });
    //POST request
    app.post("/", (req, res) => {
      //add into column task_name
      task_collection
        .insertOne(req.body)
        .then((result) => {
          res.redirect("/");
        })
        .catch((error) => console.error(error));
    });
    //Update request
    app.put("/", (req, res) => {
      task_collection
        .findOneAndUpdate(
          { name: "Bouriot Aurore" },
          {
            $set: {
              name: req.body.name,
              quote: req.body.quote,
            },
          },
          { upsert: true }
        )
        .then((result) => {
          res.json("Success");
        })
        .catch((error) => console.log(error));
    });
    //Delete request
    app.delete("/", (req, res) => {
      task_collection
        .deleteOne({ name: req.body.name })
        .then((result) => {
          if (result.deletedCount === 0) {
            return res.json("nothing to delete");
          }
          res.json("deleted");
        })
        .catch((error) => console.log(error));
    });
    //Open server
    app.listen(port, (req, res) => {
      console.log(`listening on http://localhost:${port}`);
    });
  })
  .catch((error) => console.log(error));
